# 大酱缸臭豆腐废物社交

#### Description
为了响应马桶社交，特建立此项目。
马桶出来后，还少皮搋子、洁厕灵、84等，马桶最后都通过下水道连接到化粪池等等。或者也可以叫化粪池社交，但我等是骚客，这个名字不雅；又或者叫五谷轮回之所，但有同仁指出如果轮回的是某种白色粘稠液体，这个名字就不合适了。综上所述，本人蠢且愚，暂且定名字是臭豆腐社交，全称大酱缸臭豆腐废物社交。

一首歌的歌词，道尽废物价值观：
```
矛盾 虚伪 贪婪 欺骗
幻想 疑惑 简单 善变
好强 无奈 孤独 脆弱
忍让 气愤 复杂 讨厌
嫉妒 阴险 争夺 埋怨
自私 无聊 变态 冒险
好色 善良 博爱 诡辩
能说 空虚 真诚 金钱
地狱 天堂 皆在人间
伟大 渺小 中庸 可怜
欢乐 痛苦 战争 贫寒
辉煌 暗淡 得意 伤感
怀恨 报复 专横 责难
```

#### Software Architecture
Software architecture description。或者即时通信软件，游戏引擎，等等？这样的项目也只能练手，即时通信软件己然这么多了。这里可以是各种技术的练手基地。Let's go! RK990L Mechanical https://www.rkgaming.com/


#### 这是啥
[na.support.keysight.com](http://na.support.keysight.com/pna/nvna/NVNAWebHelp/Programming/Examples/CPlus_Example.htm)

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

谷歌将使用 Windows 10 混合显示设备实现 Web VR
Google 正在考虑使用微软的 Windows 虚拟现实设备，实现基于 Chrome 的 Web VR 功能。
这意味着用户可以使用 Windows 混合现实设备浏览 Chrome 网页，目前这些功能还处于早期阶段。

根据 9 to5 Google 所说：微软内部已经将该项目作为 Windows 10 的重要项目之一，并且支持的功能将不断扩展，例如增加了对游戏和其他软件的支持。

目前 微软的 Windows 混合现实设备支持 Microsoft Edge。目前 Chrome 用户在所有浏览器中占比 65% ，如果设备支持 Chrome，这对于用户来说有很大的好处。

此外，微软在浏览器方面也作出了相应的调整，例如在近期将 Microsoft Edge 浏览器内核替换为 Chromium。微软表示，使用相同的 浏览器引擎将有利于优化 Chromium 内核，同时也为整个网络环境带来巨大的益处。

[geeksforgeeks](https://www.geeksforgeeks.org/fundamentals-of-algorithms/)

## The 10 most popular coding challenge websites for 2017
Go to the profile of Daniel Borowski
Daniel Borowski
Dec 31, 2016

A great way to improve your skills when learning to code is by solving coding challenges. Solving different types of challenges and puzzles can help you become a better problem solver, learn the intricacies of a programming language, prepare for job interviews, learn new algorithms, and more.

Below is a list of some popular coding challenge websites with a short description of what each one offers.

1. [TopCoder](https://www.topcoder.com/challenges/?pageIndex=1)

TopCoder is one of the original platforms for competitive programming online. It provides a list of algorithmic challenges from the past that you can complete on your own directly online using their code editor. Their popular Single Round Matches are offered a few times per month at a specific time where you compete against others to solve challenges the fastest with the best score.

The top ranked users on TopCoder are very good competitive programmers and regularly compete in programming competitions. The top ranked user maintains his own blog titled ALGORITHMS WEEKLY BY PETR MITRICHEV where he writes about coding competitions, algorithms, math, and more.

2. [Coderbyte](https://www.coderbyte.com/)

Coderbyte provides 200+ coding challenges you can solve directly online in one of 10 programming languages (check out this example). The challenges range from easy (finding the largest word in a string) to hard (print the maximum cardinality matching of a graph).

They also provide a collection of algorithm tutorials, introductory videos, and interview preparation courses. Unlike HackerRank and other similar websites, you are able to view the solutions other users provide for any challenge aside from the official solutions posted by Coderbyte.

3. [Project Euler](https://projecteuler.net/)

Project Euler provides a large collection of challenges in the domain of computer science and mathematics. The challenges typically involve writing a small program to figure out the solution to a clever mathematical formula or equation, such as finding the sum of digits of all numbers preceding each number in a series.

You cannot directly code on the website in an editor, so you would need to write a solution on your own computer and then provide the solution on their website.

4. [HackerRank](https://www.hackerrank.com/domains)

HackerRank provides challenges for several different domains such as Algorithms, Mathematics, SQL, Functional Programming, AI, and more. You can solve all the challenge directly online (check out this example). They provide a discussion and leaderboard for every challenge, and most challenges come with an editorial that explains more about the challenge and how to approach it to come up with a solution. Aside from the editorial, you cannot currently view the solutions of other users on HackerRank.

HackerRank also provides the ability for users to submit applications and apply to jobs by solving company-sponsored coding challenges.

5. [CodeChef](https://www.codechef.com/)

CodeChef is an Indian-based competitive programming website that provides hundreds of challenges. You are able to write code in their online editor and view a collections of challenges that are separated into different categories depending on your skill level (check out this example). They have a large community of coders that contribute to the forums, write tutorials, and take part in CodeChef’s coding competitions.

6. [CodeEval](https://www.codeeval.com/)

CodeEval is similar to HackerRank where it also provides a collection of company-sponsored coding challenges that can help you get a job if you solve them well. Companies can create challenges and host competitions to recruit new developers for work. You can see a list of current challenges here.

7. [Codewars](https://www.codewars.com/)

Codewars provides a large collection of coding challenges submitted and edited by their own community. You can solve the challenges directly online in their editor in one of several languages. You can view a discussion for each challenges as well as user solutions.

8. [LeetCode](https://leetcode.com/)

LeetCode is a popular Online Judge that provides a list of 190+ challenges that can help you prepare for technical job interviews. You can solve the challenges directly online in one of 9 programming languages. You are not able to view other users solutions, but you are provided statistics for your own solutions such as how fast your code ran when compared to other users.

They also have a Mock Interview section that is specifically for job interview preparation, they host their own coding contests, and they have a section for articles to help you better understand certain problems.

9. [SPOJ](http://www.spoj.com/)

Sphere Online Judge (SPOJ) is an online judge that provides over 20k coding challenges. You are able to submit your code in an online editor. SPOJ also hosts their own contests and has an area for users to discuss coding challenges. They do not currently provide any official solutions or editorials like some other websites do though.

10. [CodinGame](https://www.codingame.com/)

CodinGame is a bit different from the other websites because instead of simply solving coding challenges in an editor, you actually take part in writing the code for game that you play directly online. You can see a list of games currently offered here and an example of one here. The game comes with a problem description, test cases, and an editor where you can write your code in one of 20+ programming languages.

Although this website is different than typical competitive programming websites such as the ones mentioned above, it is still popular amongst programmers who enjoy solving challenges and taking part in contests.

This list was based on a few things: my own experiences using the websites, some Google searches, Quora posts, and articles such as this one and this one. I also frequented some forums and subreddits such as r/learnprogramming to see what websites were usually recommended by the users there. Disclaimer: I work at Coderbyte which is one of the websites mentioned above.

## [SCP 基金会](http://scp-wiki-cn.wikidot.com/about-the-scp-foundation)

#### C++ 网络编程库


Aggregated List of Libraries
1. [Boost.Asio](http://www.boost.org/doc/libs/release/doc/html/boost_asio.html) is really good.
1. [Asio](http://think-async.com/Asio/) is also available as a stand-alone library.
1. [ACE](http://www.dre.vanderbilt.edu/~schmidt/ACE.html) is also good, a bit more mature and has a couple of books to support it.
1. [C++ Network Library](http://cpp-netlib.org/index.html)
1. [POCO](http://pocoproject.org/)
1. [Qt](http://www.qt.io/developers/)
1. [Raknet](http://www.jenkinssoftware.com/)
1. [ZeroMQ](http://www.zeromq.org/) (C++)
1. [nanomsg](http://nanomsg.org/) (C Library)
1. [nng](https://github.com/nanomsg/nng) (C Library)
1. Berkeley Sockets
1. [libevent](http://libevent.org/)
1. [Apache APR](http://apr.apache.org/)
1. [yield](https://github.com/glycerine/yield)
1. Winsock2(Windows only)
1. [wvstreams](http://code.google.com/p/wvstreams/)
1. [zeroc](https://zeroc.com/)
1. [libcurl](http://curl.haxx.se/libcurl/)
1. [libuv](https://github.com/libuv/libuv) (Cross-platform C library)
1. [SFML's Network Module](http://www.sfml-dev.org/tutorials/2.3/#network-module)
1. [C++ Rest SDK (Casablanca)](https://github.com/microsoft/cpprestsdk)
1. [RCF](http://www.deltavsoft.com/doc/rcf_user_guide/Intro.html#rcf_user_guide.Intro.WhatIsRcf)
1. [Restbed (HTTP Asynchronous Framework)](https://github.com/corvusoft/restbed)
1. [SedNL](http://github.com/zenol/sednl)
1. [SDL_net](https://www.libsdl.org/projects/SDL_net/)
1. [OpenSplice|DDS](http://www.prismtech.com/vortex/vortex-opensplice)
1. [facil.io](http://facil.io/) (C, with optional HTTP and Websockets, Linux / BSD / macOS)
1. [GLib](https://developer.gnome.org/gio/stable/gio-gnetworking.h.html) Networking
1. [grpc](http://www.grpc.io/) from Google
1. [GameNetworkingSockets](https://github.com/ValveSoftware/GameNetworkingSockets) from Valve

#### C++ Web 服务器
1. [PHP框架 Yaf](https://github.com/laruence/yaf)Yaf是一个C语言编写的PHP框架，Yaf 的特点： 用C语言开发的PHP框架, 相比原生的PHP, 几乎不会带来额外的性能开销. 所有的框架类, 不需要编译, 在PHP启动的时候加载, 并常驻内存. 更短的内存周转周期, 提高内存利用率, 降低内存占用率. 灵巧的自动加载. 
1. [WebAppLib](https://github.com/pi1ot/webapplib)WebAppLib是一系列主要用于类Unix操作系统环境下WEB开发的C++类库。 设计目的是通过提供使用简单方便、相对独立的C++类和函数来简化CGI程序开发过程中的常见操作，提高开发效率，降低系统维护与改进的难度，适用于中等 以上规模WEB系统开发 WebAppLib所有的
1. [C++ Web工具包 Wt](https://www.webtoolkit.eu/wt)Wt(音同'witty')是一个C++库，同时也是开发和部署web应用的服务器。 Wt不是所谓框架(framework)，它只是一个库，它不会将编程方式强加于开发者。 Wt的API是以widget为中心(widget-centric)的，并受到现有C++图形用户界面(GUI) 的应用
1. [C++的Web开发框架 CppCMS](http://cppcms.com/wikipp/en/page/main)CppCMS是一个C++的Web开发框架（不是一个CMS） 。它不同于大多数其他Web开发框架，如巨蟒Django ， Java的Servlets ，或C++ Wt因为它在设计和调整来处理极高的负荷，而且它的目的是发展的网站，而不是“GUI-Like” Web应用程序。 下面是一个模板脚本示例
1. [Web 应用开发框架 TreeFrog](http://www.treefrogframework.org/)TreeFrog Framework 是一个高速的全堆栈的 Web 应用开发框架，基于 C++ 和 Qt 。Web应用程序可以运行比脚本语言更快。在应用程序的开发，它提供了一个O / R映射的系统和模板系统的MVC架构，旨在通过约定优于配置的原则，以实现
1. [新型的Web应用框架 WebMCP](https://www.public-software-group.org/webmcp)WebMCP 是一个新型的Web应用框架，采用 Lua 和 C 语言开发，与 MVC 概念不同的是，WebMCP 使用的是所谓的 Model-View-Action 概念。数据库层提供一个对象关系映射组件，HTTP-GET 请求由视图进行处理，视图处理请求数据、查询并返回查询结果。HTTP-POST 请求
1. [C语言的Web框架 Raphters](https://github.com/danielwaterworth/Raphters)Raphters 是一个C 语言的Web框架，基于 RAPHT 架构模式。包含如下模块： Resources include things served up to clients like a database or API. Actions provide ways to interact with a Resource. Processors transform data. Handlers provide the
1. [PHP扩展 HTTP extension for PHP](http://pecl.php.net/package/pecl_http)HTTP extension for PHP旨在为PHP应用提供一个方便而强大的功能扩展 。它简化了处理的HTTP网址，日期，重定向，HTTP 头信息，消息，客户的首选语言和字符集，以及提供便捷的数据高速缓存和恢复的方法。它提供了强大的功能，如果和 CURL 一起编译的话将提供
1. [C++的Web框架 runasdog](https://sourceforge.net/p/runasdog/home/main/)它是网络开发中的一个异类，开发网络应用，但是完全不需要网络接口的参与，可以将你的本地程序瞬间变成一个网络服务器，这就是runasdog！ runasdog，dog意指watchdog（看门狗），就是说它会像看门狗一样，检视着网络的一举一动，并将之通知给本地程序。目前
1. [嵌入式HTTP服务 KLone](http://www.koanlogic.com/klone/)KLone 是一个全功能的支持多平台的嵌入式HTTP服务器软件和Web开发框架，主要用在一些嵌入式设备中。
1. [兼容 WSGI 的 Web 服务器 FAPWS](http://www.fapws.org/)FAPWS (Fast Asynchronous Python WSGI Server) 是一个完全兼容 WSGI 的 Web 服务器，用于 Python 环境。设计的目标是小型快速，可处理大并发连接。
1. [用于 Web 的 C++ 编译器 Duetto](https://github.com/leaningtech)Duetto通过允许编程人员做如下的事情，来组合emscripten和node.js的优势： 用C++编写web应用，重用现有的代码，并且使得移植到浏览器的应用和游戏看起来和原生应用一样； 用同一种语言和代码库编写web应用的前端和后端 另外，duetto提供一些非常好的特性
1. [异步 Web 框架 tufao](https://github.com/vinipsmaker)Tufão 是一个C + +的异步 Web 框架，使用Qt的对象的通信系统（信号与槽）。 特性: 高性能独立服务器 跨平台支持 良好的文档 支持现代的HTTP特性 持续流 分块实体 100-continue status WebSocket 支持HTPPS 灵活的路由请求 支持条件请求的静态文件服
1. [C++14 的 Web 框架 Silicon](https://github.com/matt-42/silicon)Silicon 是一个高性能的面向中间件的 C++14 HTTP Web 开发框架，为 C++ 带来类似其他动态语言 Web 框架的便利性。 一个简单的输出 Hello world 的 JSON 接口
